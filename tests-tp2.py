from tp2 import *


def test_box_create():
	b=Box()



def test_box_add():
	b=Box()
	b.add("truc1")
	b.add("truc2")
	b.remove("truc2")


# on veut preciser si truc dans boite

def test_box_contains():
	b=Box()
	b.add("truc1")
	b.add("truc2")
	assert "truc1" in b
	assert "truc2" in b
	assert "truc3" not in b


def test_box_remove():
	b=Box()
	b.add("truc1")
	b.add("truc2")
	b.remove("truc2")
	assert "truc1" in b
	assert "truc2" not in b
	assert "truc3" not in b


def test_box_is_open():
	b=Box()
	assert b.is_open == True


def test_box_close():
	b=Box()
	b.close()
	assert b.is_open == False

def test_box_open():
	b=Box()
	b.close()
	b.open()
	assert b.is_open == True
	
	
def test_box_action_look():
	b=Box()
	b.add("truc1")
	b.add("truc2")
	assert b.action_look() == "la boite contient: truc1, truc2"
	b.close()
	assert b.action_look() == "la boite est fermee"


def test_chose_volume():
	t=Thing()
	assert t.volume() == 1
	
	
	
def test_box_capacity():
	b=Box()
	assert b._capacity == None
	b.set_capacity(9)
	assert b._capacity == 9

def test_box_has_room_for():
	b=Box()
	b.set_capacity(3)
	t=Thing(30)
	assert b.has_room_for(t.volume())==False
	b.set_capacity(100)
	assert b.has_room_for(t.volume())==True

def test_box_action_add():
	b=Box()
	t=Thing(30)
	assert b.action_add(t) == True
	b.set_capacity(3)
	assert b.action_add(t) == False
	b.set_capacity(100)
	assert b.action_add(t) == True
	b.close()
	assert b.action_add(t) == False

def test_chose_set_name():
	t=Thing()
	assert t.name=="bidule"
	t.set_name("chose")
	assert t.name=="chose"

def test_chose_has_name():
	t=Thing()
	assert t.has_name("bidule")==True
	assert t.has_name("chose")==False

def test_box_find():
	t=Thing()
	b=Box()
	b.add("bidule")
	assert b.find("bidule") == "bidule"
	assert b.find("faux")== None
	
def test_box_set_key():
	t=Thing()
	t2=Thing()	
	b=Box()
	b.set_key(t)
	b.close()
	b.open_with(t)
	assert b.is_open==True
	b.close()
	b.open_with(t2)
	assert b.is_open==False
	
	
def test_bax_thing():
	b=Box()
	assert b._poid==1
	
def test_box_in_box():
	b=Box()
	b1=Box()
	b.action_add(b1)
	assert b1 in b


def test_player():
	p=Player()
	p.name="michel"
	assert p.name=="michel"

def test_box_in_player():
	p=Player()
	b=Box(10)
	p.Boite=b
	assert (b == p.Boite)
	assert (p.Boite._capacity == 10)
	
	
def test_location():
	l=Location()
	assert (l._capacity == None)
	
	
	
def test_container():
	l=Location()
	t=Thing()
	l.action_add(t)
	assert
	
	

